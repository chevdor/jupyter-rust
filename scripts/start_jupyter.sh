#!/bin/bash
echo Setting theme to "$THEME"

# jt -t "$THEME" -f fira -fs 13 -nf ptsans -nfs 12 -N -kl -cursw 5 -cursc r -cellw 95% -T
jt -t "$THEME" -f fira -fs 13 -nf ptsans -nfs 12 -N -kl -cursw 5 -cursc r -cellw 95% -T
jupyter notebook --NotebookApp.custom_display_url=http://localhost:8888 --config=/config/jupyter/jupyter_notebook_config.py
